package org.fasttrackit;

import java.util.Scanner;

//Java program to determine puppy's adult weight; Adult weight = (Puppy weight / Puppy age in weeks) * 52
//raw food: 5% of pup weight until four months, 3% after 4 months
// no more than 5 minutes of exercise for each month of age, two times a day.
public class Main {
    public static final String AGE = "How many months old is ";
    public static final String NAME = "What is your puppy's name?  ";
    public static final String MESSAGE1 = " 's estimated adult weight in kilograms is ";
    public static final String WEIGHT = " 's current weight in kilograms? ";
    public static final String MESSAGE2 = " should eat around ";
    public static final String MESSAGE3 = " grams of kibble a day.";
    public static final String MESSAGEGIANT = "Please refer to giant breed feeding guidelines!";
    public static final String RAW = "Should you feed raw food ";
    public static final String GROWN1 = " is all grown up. How much does ";
    public static final String GROWN2 = " weigh in kilograms? ";
    public static final String ADVENTURE = " is ready for adventure. The more exercise, the better!";
    public static final String EXERCISE = " minutes of exercise twice a day.";

    public static void main(String[] args) {

        Scanner name = new Scanner(System.in);
        Scanner num1 = new Scanner(System.in);
        Scanner num2 = new Scanner(System.in);
        Scanner num3 = new Scanner(System.in);

        System.out.println(NAME);
        String pupName = name.next();

        System.out.println(AGE + pupName + "?");
        int puppyAge = num1.nextInt();
        final int PUPPYAGE = puppyAge * 4;

        int[] adultKilo = {10, 20, 30, 40, 50, 60};
        int[] pupKibble = {150, 200, 225, 260, 300, 400};

        if (puppyAge > 12) {
            System.out.println(pupName + GROWN1 + pupName + GROWN2);
            double grownUpWeight = num3.nextDouble();
            int[] grownWeight = {10, 20, 30, 40, 50, 60, 70};
            int[] grownKibble = {170, 220, 270, 320, 370, 420, 470};
            double rawPup1;
            rawPup1 = (grownUpWeight * 3) / 100;
            System.out.println(RAW + pupName + " will eat " + (rawPup1 * 1000) + " grams.");
            for (int i = 0; i < grownWeight.length; i++) {
                if (grownUpWeight <= grownWeight[i]) {
                    System.out.println(pupName + MESSAGE2 + grownKibble[i] + MESSAGE3);
                    break;
                }

            }

        } else {
            System.out.println(pupName + WEIGHT);
            double puppyWeight = num2.nextDouble();
            double adultWeight = (puppyWeight / PUPPYAGE) * 52;
            double rawPup2;
            rawPup2 = (puppyWeight * 5) / 100;
            System.out.println(RAW + pupName + " will eat " + (rawPup2 * 1000) + " grams.");
            System.out.println(pupName + MESSAGE1 + adultWeight);
            if (adultWeight > adultKilo[5]) {
                System.out.println(MESSAGEGIANT);
            } else {
                for (int i = 0; i < adultKilo.length; i++) {
                    if (adultWeight <= adultKilo[i]) {
                        System.out.println(pupName + MESSAGE2 + pupKibble[i] + MESSAGE3);
                        break;
                    }

                }
            }
        }

        if (puppyAge > 12) {
            System.out.println(pupName + ADVENTURE);
        } else {
            int[] minutePerDay = {10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120};
            int exerciseNeed = puppyAge * 5;
            for (int i = 0; i < minutePerDay.length; i++) {
                if (exerciseNeed <= minutePerDay[i]) {
                    System.out.println(pupName + " needs " + minutePerDay[i] + EXERCISE);
                    break;
                }
            }
        }

    }
}